package Desafios_Lab;

import java.util.Random;

public class desafio_11 {

    public static void main(String[] args) {

        CuentaCorriente cta1 = new CuentaCorriente("Diego ", 5000);
        CuentaCorriente cta2 = new CuentaCorriente("Stefania",3000);

        CuentaCorriente.Tranferencia(cta1,cta2,500);

        System.out.println("\n"+cta1.toString());
        System.out.printf("\n");
        System.out.println(cta2.toString());


    }
}
  class CuentaCorriente {

      private double saldo;
      private String nombre_titular;
      private long numeroCuenta;

      public CuentaCorriente(String nombre_titular, double saldo) {
          this.nombre_titular = nombre_titular;
          this.saldo = saldo;

          Random aleatorio = new Random();
          this.numeroCuenta = Math.abs(aleatorio.nextLong());


      }

      public String ingresar_Dinero(double dinero) {
          if (dinero > 0) {
              this.saldo += dinero;
              return "Se realizo con Exito la transaccion";
          } else {
              return "No se puede realizar la operacion por que el numero es negativo";

          }
      }
      public void sacarDinero (double dinero){
          this.saldo-=dinero;
      }

      public double getSaldo() {
          return saldo;
      }
// metodo toString
      @Override
      public String toString() {
          return "CuentaCorriente:\n" +"saldo =\t" + saldo +"\nnombre_titular=\t" + nombre_titular + '\n' +"numeroCuenta=\t" + numeroCuenta;
      }
      //esta es la realizacion de la transferencia .

      public static void Tranferencia (CuentaCorriente ctaSalida, CuentaCorriente ctaEntrada , double dinero ){

          ctaEntrada.saldo+=dinero;
          ctaSalida.saldo-=dinero;
      }

  }