package Desafios_Lab;

import java.util.Scanner;

public class Desafio13_prog05 {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);


        int matriz[][] = new int[3][3];

        System.out.printf("\n\t----------| Empecemos a rellenar la Matriz |----------\n");

        System.out.println("__________Ingrese los numeros para rellenar la matriz___________\n");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                matriz[i][j] = leer.nextInt();

                System.out.println(matriz[i][j]+ " ");
            }
        }
        System.out.println(" ");

        System.out.printf("******** La Matriz es:******** \n");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                System.out.print("\t\t"+ matriz[i][j]+" ");

            }System.out.println(" ");
        }

        for (int y = 0; y < matriz.length; y++) {

            for (int z = 0; z < matriz.length; z++) {

                for (int p = 0; p < matriz.length; p++) {

                    for (int t = 0; t < matriz.length; t++) {

                        if (matriz[y][z] <= matriz[p][t]) {
                            matriz[y][z] = matriz[y][z];

                        } else {

                            int aux = matriz[p][t];
                            matriz[p][t] = matriz[y][z];
                            matriz[y][z] = aux;
                        }
                    }
                }
            }
        }
        System.out.printf("\n******** La Matriz Ordenada de Mayor a Menor es:******** \n");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                System.out.print("\t\t"+ matriz[i][j]+" ");

            }System.out.println(" ");
        }
    }
}