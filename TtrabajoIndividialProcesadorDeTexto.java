package Procesador_De_Texto;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

import javax.swing.text.*;


public class TtrabajoIndividialProcesadorDeTexto  {

    public static void main(String[] args) {

        MenuProcesadorII mimarco = new MenuProcesadorII();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



    }
}
//-------------------------------------------------------------------------------------------------------

class MenuProcesadorII extends JFrame{

    public MenuProcesadorII(){

        setTitle("-------------------------- Editor de texto   Diego Rios ----------------- ");
        setBounds(500,300,550,400);
        laminaprocesadorII milamina = new laminaprocesadorII();
        add(milamina);

        setVisible(true);
    }
}



//-------------- LAMINAS ----------------------------------------------------------------------------------------

class laminaprocesadorII extends JPanel {

    public laminaprocesadorII() {           //Constructor   de la clase

        setLayout(new BorderLayout());

        JPanel laminamenu = new JPanel(); // Establecemos la barra donde iria la barra de menus

        JMenuBar mibarra = new JMenuBar();  // Establecemos la barra de manus


        fuente  = new JMenu( "Fuente");
        estilo  = new JMenu( "Estilo");
        tamanio = new JMenu( "Tamanio");
        abrir   = new JMenu(  "Abrir");
        guardar = new JMenu( "Guardar");


        mibarra.add(fuente);  // Cargamos en fuente lo que queremos que cuelgue cuando hacemos clic
        mibarra.add(estilo);
        mibarra.add(tamanio);
        mibarra.add(abrir);
        mibarra.add(guardar);




//---------------- BOTONES  FUENTE --------------------------------------------------------------------------------------

        configura_menu("Arial"   , "Fuente" , "Arial"     ,9 ,10);
        configura_menu("Courier" , "Fuente" , "Courier"   ,9 ,10);
        configura_menu("Verdana" , "Fuente" , "Verdana"   ,9 ,10);

//---------------- BOTONES ESTILO --------------------------------------------------------------------------------------

        configura_menu("Negrita" , "Estilos" , ""  ,Font.BOLD   ,1);
        configura_menu("Cursiva" , "Estilos" , ""  ,Font.ITALIC ,1);

//---------------- BOTONES ESTILO --------------------------------------------------------------------------------------
// Nota: el valor 9 no corresponde a niun dato preciso en tamnio por eso lo ponemos

        configura_menu("12", "tam" ," "  ,9 ,12);
        configura_menu("16", "tam" ," "  ,9 ,16);
        configura_menu("20", "tam" ," "  ,9 ,20);
        configura_menu("24", "tam" ," "  ,9 ,24);

        // Nota estos botones van con el nombremiento del if donde hacemos la verificacion de cada condicion ###
//----------------------------------------------------------------------------------------------------------------------



        laminamenu.add(mibarra);
        add(laminamenu , BorderLayout.NORTH);

        miarea = new JTextPane();
        add(miarea, BorderLayout.CENTER);

    } // llave constructor

//********************************************************************************************************************

    public void configura_menu (String rotulo, String menu, String tipo_letra , int estilos , int tam){

        JMenuItem elem_menu = new JMenuItem(rotulo);

        if(menu == "Fuente"){
            fuente.add(elem_menu);
            if (tipo_letra == "Arial"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Arial"));

            }else if (tipo_letra == "Courier"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Courier"));

            }else if (tipo_letra == "Verdana"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Verdana"));
            }

        }else if (menu == "Estilos"){
            estilo.add(elem_menu);
            if (estilos == Font.BOLD){
                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,InputEvent.CTRL_DOWN_MASK));
                elem_menu.addActionListener(new StyledEditorKit.BoldAction());

            }else if (estilos == Font.ITALIC){
                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));

                elem_menu.addActionListener(new StyledEditorKit.ItalicAction());

            }

        }else if (menu == "tam"){

            tamanio.add(elem_menu);

            elem_menu.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio",tam));
        }
    }





//********************************************************************************************************************
    JTextPane miarea;
    JMenu fuente, estilo , tamanio , abrir ,guardar ;
    Font letras;
}




