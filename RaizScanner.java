package Desafios_Lab;

import java.util.Scanner;

public class RaizScanner {

    public static void main(String[] args) {

        Scanner leer = new Scanner(System.in);

        int numero;
        double raiz;

        System.out.print("Ingrese un numero para realizar la Raiz Cuadrada:\n");
        numero = leer.nextInt();

        raiz = Math.sqrt(numero);

        System.out.print("El numero ingresado es:"+numero+"\n"+"Y su raiz cuadrada es:"+raiz );

    }
}