package TGN2;

public abstract class Persona {

    //---------------------------Atributos
    private String Nombre;
    private int Edad;


//-------------------------- Getters and Setters

   public String getNombre (){
       return  Nombre;
   }

    public void setNombre(String nombre) {
        this.Nombre = nombre;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int edad) {
        this.Edad = edad;
    }

    public abstract String getTipo();


//---------------------------Metodo Tostring (mostramos lo que queremos de la clase)

    @Override
    public abstract String toString();
}
