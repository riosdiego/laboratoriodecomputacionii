package TGN2;

import javafx.scene.effect.Effect;

import java.util.Scanner;

public class Cine {
    public static void main(String[] args) {
        Espectadores[] espectadores = new Espectadores[3]; // creo un arreglo  donde cargamos los espectadores.

        for (int i = 0; i < espectadores.length; i++) { // contabilizo la carga de los espectadores .


            try {     // metodo try catch  para la corroboracion del ingreso de datos .

                Scanner leer = new Scanner(System.in);
//--------------------------------------------------------------  INGRESO DE DATOS .
                System.out.printf("Ingrese su Nombre:\n");
                String Nombre = leer.nextLine();

                System.out.printf("Ingrese su Edad:\n");
                int Edad = leer.nextInt();

                System.out.printf("Ingrese su Fila:\n");
                String Fila = leer.next();

                System.out.printf("Ingrese su Silla:\n");
                int Silla = leer.nextInt();

                espectadores[i] = new Espectadores(Nombre, Edad, Fila, Silla);  // carga de datos al arreglo ya creado anteriormente .


                Salas salas01 = new Salas(3, "Sala01");
                salas01.setPeliculas("Joker");

                salas01.setEspecatadores(espectadores);
                System.out.printf(salas01.getEspectadores());

                Acomodadores acomodador = new Acomodadores("Arnaldo", 99);
                acomodador.setSala(salas01);
                acomodador.setSueldo(50.000);

                acomodador.toString();

                Empleados empleado = new Empleados("Anastacio", 58);
                empleado.toString();


            } catch (Exception e) {  // despues de la corrboracion imprimimos si hay algun error un mensaje de aviso .
                System.out.printf("\nAlguno de los datos fueron ingresados incorrectamente ! ");


            }
        }

    }
}

