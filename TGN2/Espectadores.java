package TGN2;

public class Espectadores extends Persona {

    private String Fila;
    private  int Silla ;

    public Espectadores (String nombre, int edad ,String fila,int silla){
        super.setNombre(nombre);
        super.setEdad(edad);
        this.Fila = fila;
        this.Silla = silla;
    }
    public String getAsiento(){
        return this.getNombre()+"--"+ this.getEdad()+"--"+this.Fila+"--"+this.Silla;
    }

    @Override
    public String getTipo() {
        return null;
    }

    @Override
    public String toString() {
        return null ;
    }
}
