package TGN2;


import java.util.Arrays;


public class Salas {
    //---------------------------------------Atributos
    private int Capacidad;
    private String Peliculas;
    private String Nombre;
    //---------------------------------------Arreglo de clas espectaodores
    private Espectadores[] espectadores;


//----------------------------------------Constructor


    public Salas(int capacidad, String nombre) {
        this.Capacidad = capacidad;
        this.Nombre = nombre;
    }


//---------------------------------------getters and setters

    public void setPeliculas(String peliculas) {
        this.Peliculas = peliculas;
    }


    //----------------------------------------- aca hago el control de espectadores cargados para que no supere la capacidad de la sala.
    public void setEspecatadores(Espectadores[] espectadores) {
        if (espectadores.length > this.Capacidad) {
            System.out.printf("Lo Sentimos, La Capacidad supera el MAXIMO Permitido");
        } else {
            this.espectadores = espectadores.clone();
        }
    }
//------------------------------------ en este proceso  analizamos algun error a la carga de espectadores,
//                                    si hubiera algun erros automaticamente saltaria el msj establecido.

    public String getEspectadores(){
        try {
            String listaEspectadores = "Espectadores:\t";
            for (Espectadores espectador: this.espectadores){
                listaEspectadores = listaEspectadores + espectador.toString();
            }
            return  listaEspectadores;
        }catch (Exception e){
            return  "No Se Encontraron Espectadores Cargados";
        }
    }

//--------------------------------------Metodo toString (Mostramos lo que contiene la clase)

    @Override
    public String toString() {
        return "Salas{" +
                "Capacidad=" + Capacidad +
                ", Peliculas='" + Peliculas + '\'' +
                ", Nombre='" + Nombre + '\'' +
                ", especatadores=" + Arrays.toString(espectadores) +
                '}';
    }
}
