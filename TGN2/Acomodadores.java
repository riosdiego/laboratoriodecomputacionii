package TGN2;

public class Acomodadores extends  Empleados implements ParaAcomodadores {

    private Salas sala;


    public Acomodadores(String nombre, int edad) {
        super(nombre, edad);
    }

    public Salas getSala(){
        return this.sala;
    }

    @Override
    public Salas getsala() {
        return null;
    }

    public void setSala(Salas sala) {
            this.sala = sala;
    }

    public String getTipo(){
        return "Acomodador";
    }

    @Override
    public String toString() {
        return "Acomodador:"+getNombre()+"--"+"sala=" + sala;
    }
}