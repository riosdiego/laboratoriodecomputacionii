package TGN2;

public class Empleados extends Persona {

//---------------------------- Atributos

    private Double Sueldo;
    // tipo se refiere al cargo que tiene , sea empleado del cine o civil comun.

    //---------------------------- Metodos

    public Empleados(String nombre, int edad) {
        super.setNombre(nombre);
        super.setEdad(edad);
    }

//----------------------------- Getters and Setters

    public void setSueldo(Double sueldo) {
    this.Sueldo= sueldo;
    }



//-----------------------------Metodo Tostring (Muestro lo que necesito de clase )
    @Override
    public String getTipo() {
        return "Empleado";
    }

    @Override
    public String toString() {
        return "Empleados" +"--"+"Sueldo=" +"--"+ Sueldo +"--"+"Tipo='" +"--"+ getTipo() + '\'' +
                '}';
    }
}
